""" plombot """
import discord
from discord.ext import commands

import keys

def get_prefix(bot, message):
    return ";"

class MyBot(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=get_prefix)
        # Load cogs
        self.load_extension('cogs.example_cog')

        @self.event
        async def on_ready():
            """ Called after the bot successfully connects to Discord servers """
            print(f"Connected as {self.user.display_name}")

            # Change presence to "Hanging music in 69 guilds"
            text = f"Hanging out in {len(self.guilds)} guilds"
            activity = discord.Activity(name=text, type=discord.ActivityType.streaming)
            await self.change_presence(activity=activity) 

        @self.event
        async def on_guild_join(guild):
            """ Called when a bot joins a guild """
            pass


def main():
    bot = MyBot("!")
    bot.run(keys.discord_token)

if __name__ == '__main__':
    main()
