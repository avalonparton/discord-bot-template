""" Example discord.ext.commands extension """
import discord
from discord.ext import commands

class ExampleCog(commands.Cog):
    """ Example cog

    Provides commands:
         ;hello
    """
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def hello(self, ctx):
        """Sends hello world message """
        await ctx.send(f"Hello, {ctx.author.display_name}!")

def setup(bot):
    print("Loading ExampleCog")
    bot.add_cog(ExampleCog(bot))
    print("Loaded ExampleCog")
